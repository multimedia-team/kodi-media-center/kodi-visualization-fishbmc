kodi-visualization-fishbmc (21.0.2+ds-1) unstable; urgency=high

  * New upstream version 21.0.2+ds
  * d/copyright: Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 27 Jan 2025 08:47:22 +0000

kodi-visualization-fishbmc (20.2.0+ds1-3) unstable; urgency=critical

  * Mass fix: d/watch: Allow multiple addon suites to be checked

 -- Vasyl Gello <vasek.gello@gmail.com>  Tue, 20 Aug 2024 12:15:13 +0000

kodi-visualization-fishbmc (20.2.0+ds1-2) unstable; urgency=medium

  * Branch out experimental
  * Modernize package
  * New release 20.1.0+ds1-1
  * Finalize changelog
  * d/gbp.conf: Fix debian and upstream branches
  * Import Debian packaging from debian/sid
  * Prepare for v21 "Omega"
  * Bump copyright years
  * d/control: Ensure Vcs-Git points to correct branch
  * Remove empty patch series
  * Bump standards version to 4.7.0 (no changes required)

 -- Vasyl Gello <vasek.gello@gmail.com>  Fri, 09 Aug 2024 08:47:18 +0000

kodi-visualization-fishbmc (20.2.0+ds1-1) unstable; urgency=medium

  [ Vasyl Gello ]
  * New upstream version 20.2.0+ds1
  * Drop patches merged upstream

  [ Mattia Rizzolo ]
  * Update copyright.

 -- Vasyl Gello <vasek.gello@gmail.com>  Fri, 16 Dec 2022 12:21:14 +0000

kodi-visualization-fishbmc (20.1.0+ds1-2) unstable; urgency=medium


  [ Vasyl Gello ]
  * Fix lintian warnings
  * d/watch: switch to git tags

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete field Contact from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Vasyl Gello ]
  * d/patches: Add unmerged upstream PR fixing build

 -- Vasyl Gello <vasek.gello@gmail.com>  Sat, 15 Oct 2022 16:54:25 +0000

kodi-visualization-fishbmc (20.1.0+ds1-1) unstable; urgency=medium

  * New upstream version 20.1.0+ds1
  * Prepare for v20 in unstable

 -- Vasyl Gello <vasek.gello@gmail.com>  Thu, 04 Aug 2022 09:54:29 +0000

kodi-visualization-fishbmc (19.0.1+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.1+ds1
  * Modernize package

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 21 Mar 2022 17:48:37 +0000

kodi-visualization-fishbmc (19.0.0+ds1-1) unstable; urgency=medium

  * New upstream version 19.0.0+ds1
  * Bump standard version
  * Disable LTO to make build reproducible

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 13 Oct 2021 22:14:51 +0000

kodi-visualization-fishbmc (6.3.0+ds1-3) unstable; urgency=medium

  * Fix github ref
  * Restrict watchfile to current stable Kodi codename

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 16 Aug 2021 05:40:13 +0000

kodi-visualization-fishbmc (6.3.0+ds1-2) unstable; urgency=medium

  * Force override libdir paths
  * Bump copyright years

 -- Vasyl Gello <vasek.gello@gmail.com>  Wed, 20 Jan 2021 06:24:18 +0000

kodi-visualization-fishbmc (6.3.0+ds1-1) unstable; urgency=medium

  * Initial upload

 -- Vasyl Gello <vasek.gello@gmail.com>  Mon, 28 Dec 2020 07:58:58 +0000
